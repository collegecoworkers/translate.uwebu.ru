<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Перевод текста</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="assets/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<!-- Static navbar -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="">Перевод текста</a>
				</div>
			</div>
		</nav>
		<form id="form" onsubmit="return false">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group ">
						<label class="control-label">Переводить с</label>
						<select name="move_city" id="from" class="form-control">
							<option value="ru">Русский</option>
							<option value="en">Английский</option>
						</select>
					</div>
					<div class="form-group ">
						<label class="control-label">Текст</label>
						<textarea class="form-control" rows="5" placeholder="" id="text" name="experience" required></textarea>
					</div>
					<div class="form-group ">
						<label class="control-label">Переводить на</label>
						<select name="move_city" id="to" class="form-control">
							<option value="en">Английский</option>
							<option value="ru">Русский</option>
						</select>
					<div class="form-group ">
						<label class="control-label">Перевод</label>
						<textarea class="form-control" id="translated_text" rows="5" placeholder="translated text" ></textarea>
					</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="button" id="translate" class="btn btn-success">Перевести</button>
				</div>
			</div>
		</form>
		<br><br><br><br><br>
	</div>
</body>
</html>

<script>
	var yatr = {
		key : 'trnsl.1.1.20171117T190404Z.3a0f6041cd0bc8b4.dffbe5f962c8e543cc0221e6c10852626bb438ef',
		api : 'https://translate.yandex.net/api/v1.5/tr.json/translate?',
		translate : function(text, from, to, callback){
			var url = this.api;
			url += '&key=' + this.key;
			url += '&text=' + encodeURIComponent(text);
			url += '&lang=' + from + '-' + to;

			var ajax = new XMLHttpRequest();
			ajax.open('GET', url, true);
			ajax.onreadystatechange = function(){
				if(ajax.readyState == 4){
					if(ajax.status == 200){
						text = JSON.parse(ajax.responseText);
						callback(text.text[0]);
					}
				}
			};
			ajax.send(null);
		},
	};

	document.getElementById('translate').onclick = function(){
		var from = document.getElementById('from').value;
		var to = document.getElementById('to').value;
		var text = document.getElementById('text').value;
		var translated_text = document.getElementById('translated_text');

		yatr.translate(text, from, to, function(text){
			translated_text.value = text;
		});

	}
</script>
